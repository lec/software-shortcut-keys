package vietnamsoft.shortcut.utl;

public class DataUtil {
	public static String getFirstLetterUpperString(String st, int position)
	{
		String rs = "";
		for(int c=position;c<st.length();c++)
		{
			if(Character.isLetter(st.charAt(c)))
			{
				rs = Character.toString(st.charAt(c)).toUpperCase();
				break;
			}
		}
		return rs;
	}
}
