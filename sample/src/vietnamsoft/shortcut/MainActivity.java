/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vietnamsoft.shortcut;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import net.simonvt.menudrawer.MenuDrawer;

import org.apache.commons.lang.SerializationUtils;

import vietnamsoft.shortcut.listviewfilter.SectionListAdapter;
import vietnamsoft.shortcut.listviewfilter.SectionListView;
import vietnamsoft.shortcut.utl.DataUtil;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.shortcut.vo.Platform;
import com.shortcut.vo.ShortcutKey;
public class MainActivity extends FragmentActivity {
	//listview section
    private StandardArrayAdapter arrayAdapter;
    private SectionListAdapter sectionAdapter;
    private SectionListView listView;
	
    EditText search;
	
	//sideIndex
	LinearLayout sideIndex;	
	// height of side index
    private int sideIndexHeight,sideIndexSize;
    // list with items for side index
    private ArrayList<Object[]> sideIndexList = new ArrayList<Object[]>();
    private ArrayList<String> shortcutKeys;
    //static String[] COUNTRIES_ARY = new String[10];
	private static final String STATE_ACTIVE_POSITION = "net.simonvt.menudrawer.samples.ContentSample.activePosition";
    private static final String STATE_CONTENT_TEXT = "net.simonvt.menudrawer.samples.ContentSample.contentText";
    private MenuDrawer mMenuDrawer;
    private MenuAdapter mAdapter;
    private ListView mList;
    private int mActivePosition = -1;
    private String mContentText;
    private TextView mContentTextView;
	
	private final Handler handler = new Handler();
	List<Platform> platforms = null;
	Map<String, List<Platform>> groupType = new HashMap<String, List<Platform>>();
	private Drawable oldBackground = null;
	private int currentColor = 0xffcc00;
	Map<String, List<Map<String, Object>>> mapGr = new HashMap<String, List<Map<String, Object>>>();
	Map<String, List<ShortcutKey>> mapKeys = null;
	private Object getDatFile(int id) throws IOException
	{
		InputStream ins = getResources().openRawResource(id);
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
		int size = 0;
		// Read the entire resource into a local byte buffer.
		byte[] buffer = new byte[1024];
		while((size=ins.read(buffer,0,1024))>=0){
		  outputStream.write(buffer,0,size);
		}
		//ins.close();
		buffer=outputStream.toByteArray();
		Object ob = SerializationUtils.deserialize(buffer);
		return ob;
	}
	private String getTypeNameById(int id)
	{
		String name = "All";
		if (id == 4)
		{
			name = "Operating System";
		}
		if (id == 5)
		{
			name = "Application";
		}
		if (id == 6)
		{
			name = "Game";
		}
		return name;
	}
	@SuppressWarnings("unchecked")
	private void loadTitles() throws IOException
	{
		platforms = (List<Platform>)getDatFile(R.raw.platformfull);
		//generate platforms with their group
        for (Platform item : platforms)
        {
            if (groupType.get(item.getPlatformType()) == null)
            {
                List<Platform> lst = new ArrayList<Platform>();
                lst.add(item);
                groupType.put(item.getPlatformType(), lst);
            }
            else
            {
                List<Platform> lst = groupType.get(item.getPlatformType());
                lst.add(item);
                groupType.put(item.getPlatformType(), lst);
            }
        }
        mapKeys = (Map<String, List<ShortcutKey>>)getDatFile(R.raw.keydb);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			loadTitles();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Random ran = new Random();
		int k = ran.nextInt();
		k = k%7;
		int color = Color.parseColor("#f56e1d");
		changeColor(color);
	    if (savedInstanceState != null) {
            mActivePosition = savedInstanceState.getInt(STATE_ACTIVE_POSITION);
            mContentText = savedInstanceState.getString(STATE_CONTENT_TEXT);
        }

        mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.MENU_DRAG_CONTENT);
        mMenuDrawer.setContentView(R.layout.activity_contentsample);
        mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_FULLSCREEN);

        
        List<Object> items = new ArrayList<Object>();
        Iterator<Entry<String, List<Platform>>> ite = groupType.entrySet().iterator();
        while(ite.hasNext())
        {
            Entry<String, List<Platform>> item = ite.next();
            items.add(new Category(getTypeNameById(Integer.parseInt(item.getKey()))));
            Collections.sort(item.getValue(), new Comparator<Platform>(){
                public int compare(Platform o1, Platform o2){
                	return o1.getPlatformName().compareTo(o2.getPlatformName());
                }
           });
            for (Platform st : item.getValue())
            {
            	String name = st.getIconUri().toLowerCase();
            	name = name.replaceAll("-", "_");
            	name = name.substring(0, name.length() - 4);
            	String cr[] = name.split("\\.");
            	if (cr.length == 2)
            	{
            		name = cr[0];
            	}
            	int id = getResources().getIdentifier(name,"drawable", getPackageName());
            	
            	items.add(new Item(st.getPlatformName(), id));
            }
            
        }
        
        mList = new ListView(this);
        mAdapter = new MenuAdapter(items);
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(mItemClickListener);

        mMenuDrawer.setMenuView(mList);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContentTextView = (TextView) findViewById(R.id.contentText);
        mContentTextView.setText(mContentText);
        loadListViewWithAutocomplete("4", "Windows shortcut keys");
	}
	private class Indextouch implements OnTouchListener 
    {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

         	
         	if(event.getAction() ==MotionEvent.ACTION_MOVE  || event.getAction() ==MotionEvent.ACTION_DOWN)
         	{
         		 
         		 sideIndex.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_rectangle_shape));
         		
         		 // now you know coordinates of touch
                 float  sideIndexX = event.getX();
                 float  sideIndexY = event.getY();

                  if(sideIndexX>0 && sideIndexY>0)
                  {
                  	 // and can display a proper item it country list
                      displayListItem(sideIndexY);
                  	
                  }
         	}
         	else
         	{
         		sideIndex.setBackgroundColor(Color.TRANSPARENT);
         	}
            

             return true;
         
		}
    	
    };
   
    public void onWindowFocusChanged(boolean hasFocus)
	{
    	 // get height when component is poplulated in window
		 sideIndexHeight = sideIndex.getHeight();
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 super.onWindowFocusChanged(hasFocus);
	}  
	  
	  
		private class StandardArrayAdapter extends BaseAdapter implements Filterable
	    {

	        private final ArrayList<String> items;

	        public StandardArrayAdapter(ArrayList<String> args) 
	        {
	            this.items = args;
	        }
	      
	        @Override
	        public View getView(final int position, final View convertView, final ViewGroup parent) 
	        {
	            View view = convertView;
	            if (view == null)
	            {
	                final LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	                view = vi.inflate(R.layout.row, null);
	            }
	            TextView textView = (TextView)view.findViewById(R.id.row_title);
                if (textView != null) 
                {
                    textView.setText(Html.fromHtml(items.get(position)));
                }
	            return view;
	        }

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return items.size();
			}

			@Override
			public Filter getFilter() {
				Filter listfilter=new MyFilter();
				return listfilter;
			}

			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return items.get(position);
			}

			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return 0;
			}
	    }

		
		public class MyFilter extends Filter
		{

			@Override
			protected FilterResults performFiltering(CharSequence constraint)
			{
				// NOTE: this function is *always* called from a background thread, and
	            // not the UI thread.
	            constraint = search.getText().toString();
	            FilterResults result = new FilterResults();
	            if(constraint != null && constraint.toString().length() > 0)
	            {
	            	//do not show side index while filter results
	            	runOnUiThread(new Runnable() {
						
						@Override
						public void run() 
						{
							((LinearLayout)findViewById(R.id.list_index)).setVisibility(View.INVISIBLE);
						}
					});
	        
	               ArrayList<String> filt=new ArrayList<String>();
	               ArrayList<String> Items=new ArrayList<String>();
	                synchronized(this)
	                {
	                    Items = shortcutKeys;
	                }
	                for(int i = 0;i<Items.size(); i++)
	                {
	                	String item = Items.get(i);
	                   if(item.toLowerCase().startsWith("<i>"+constraint.toString().toLowerCase()))
	                   {
	                    	 	filt.add(item);
	                   }
	                }
	                
	                result.count = filt.size();
	                result.values = filt;
	            }
	            else
	            {
	            	
	            	runOnUiThread(new Runnable() {
						
						@Override
						public void run() 
						{
					    	((LinearLayout)findViewById(R.id.list_index)).setVisibility(View.VISIBLE);
						}
					});
	                synchronized(this)
	                {
	                    result.count = shortcutKeys.size();
	                    result.values = shortcutKeys;
	                }
	            	
	            }
	            return result;
			}

			@Override
			protected void publishResults(CharSequence constraint,FilterResults results) 
			{
					@SuppressWarnings("unchecked")
					ArrayList<String> filtered = (ArrayList<String>)results.values;
					arrayAdapter=  new StandardArrayAdapter(filtered);
					sectionAdapter = new SectionListAdapter(getLayoutInflater(),arrayAdapter);
			        listView.setAdapter(sectionAdapter);
				  
			}
			
		}
	    
	  
	    private void displayListItem(float sideIndexY)
	    {
	        // compute number of pixels for every side index item
	        double pixelPerIndexItem = (double) sideIndexHeight / sideIndexSize;

	        // compute the item index for given event position belongs to
	        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

	        if(itemPosition<sideIndexList.size())
	        {
	     	   // get the item (we can do it since we know item index)
	            Object[] indexItem = sideIndexList.get(itemPosition);
	            listView.setSelectionFromTop((Integer)indexItem[1], 0);
	        }
	    }
	   
	    @SuppressLint("DefaultLocale")
		private void PoplulateSideview()
	    {
	    		
	    		String latter="";
	    		String latter_temp = "";
	    		int index=0;
	    		sideIndex.removeAllViews();
	    		sideIndexList.clear();
	    		for(int i=0;i<shortcutKeys.size();i++)	    		{
	    			Object[] temp=new Object[2];
	    			/*String lat = COUNTRIES.get(i);
	    			for(int c=2;c<lat.length();c++)
	    			{
	    				if(Character.isLetter(lat.charAt(c)))
	    				{
	    					latter_temp = Character.toString(lat.charAt(c)).toUpperCase();
	    					break;
	    				}
	    			}*/
	    			latter_temp = DataUtil.getFirstLetterUpperString(shortcutKeys.get(i), 2);
	    			//latter_temp=(COUNTRIES.get(i)).substring(0, 1).toUpperCase();
	    			if(!latter_temp.equals(latter))
	    			{
	    				// latter with its array index
	    				latter=latter_temp;
	    				temp[0]=latter;
	    				temp[1]=i+index;
	    				index++;
	    				sideIndexList.add(temp);
	    				
	    				TextView latter_txt=new TextView(this);
	    				latter_txt.setText(latter);
	    				
	    				latter_txt.setSingleLine(true);
	    				latter_txt.setHorizontallyScrolling(false);
	    				latter_txt.setTypeface(null, Typeface.BOLD);
	    				latter_txt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,getResources().getDimension(R.dimen.index_list_font));
	    				LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,1);
	    				params.gravity=Gravity.CENTER_HORIZONTAL;    
	    				
	    				latter_txt.setLayoutParams(params);
	    				latter_txt.setPadding(10, 0,10, 0);
	    				
	    				
	    				sideIndex.addView(latter_txt);
	    			}
	    		}
	    		
	    		sideIndexSize=sideIndexList.size();
	    		
	    }

		private TextWatcher filterTextWatcher = new TextWatcher() 
		   {
				
		    public void afterTextChanged(Editable s)
		    {
		    	new StandardArrayAdapter(shortcutKeys).getFilter().filter(s.toString());
		    }

		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }

		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        // your search logic here
		    }

		}; 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.action_contact:
			QuickContactFragment dialog = new QuickContactFragment();
			dialog.show(getSupportFragmentManager(), "QuickContactFragment");
			return true;
		case android.R.id.home:
            mMenuDrawer.toggleMenu();
		} 

		return super.onOptionsItemSelected(item);
	}
	private void loadListViewWithAutocomplete(String id, String label)
	{
		search=(EditText)findViewById(R.id.search_query);
		search.addTextChangedListener(filterTextWatcher);
		listView = (SectionListView) findViewById(R.id.section_list_view);
		sideIndex = (LinearLayout) findViewById(R.id.list_index);	
		sideIndex.setOnTouchListener(new Indextouch());
		
		mContentTextView.setText(label);
		List<ShortcutKey> refKeys = mapKeys.get(id);
		//mItems = new ArrayList<Spanned>();
		ArrayList<String> refSt = new ArrayList<String>();
		shortcutKeys = new ArrayList<String>();
        if (refKeys != null && refKeys.size() > 0)
        {
        	for(ShortcutKey item : refKeys)
        	{
        		refSt.add("<i>" + item.getShortcutName() + " </i>  <br/><b><u>" + item.getShortcutValue() + "</u></b>");
        		shortcutKeys.add("<i>" + item.getShortcutName() + " </i>  <br/><b><u>" + item.getShortcutValue() + "</u></b>");
        	}
        }
        
        Collections.sort(refSt);
        Collections.sort(shortcutKeys);
        if(refSt.size()>0)
		{
        	arrayAdapter =new StandardArrayAdapter(refSt);
        	
        	//adaptor for section
	        sectionAdapter = new SectionListAdapter(this.getLayoutInflater(),arrayAdapter);
	        listView.setAdapter(sectionAdapter);
	        
	        
	        PoplulateSideview();
		}
	}
	private void changeColor(int newColor) {

		int color = Color.parseColor("#f56e1d");
		oldBackground = new ColorDrawable(color);
		// change ActionBar color just if an ActionBar is available
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

			Drawable colorDrawable = new ColorDrawable(newColor);
			Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
			LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

			if (oldBackground == null) {

				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
					ld.setCallback(drawableCallback);
				} else {
					getActionBar().setBackgroundDrawable(ld);
				}

			} else {

				TransitionDrawable td = new TransitionDrawable(new Drawable[] { oldBackground, ld });

				// workaround for broken ActionBarContainer drawable handling on
				// pre-API 17 builds
				// https://github.com/android/platform_frameworks_base/commit/a7cc06d82e45918c37429a59b14545c6a57db4e4
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
					td.setCallback(drawableCallback);
				} else {
					getActionBar().setBackgroundDrawable(td);
				}

				td.startTransition(200);

			}

			oldBackground = ld;

			// http://stackoverflow.com/questions/11002691/actionbar-setbackgrounddrawable-nulling-background-from-thread-handler
			getActionBar().setDisplayShowTitleEnabled(false);
			getActionBar().setDisplayShowTitleEnabled(true);

		}

		//currentColor = newColor;

	}

	public void onColorClicked(View v) {

		int color = Color.parseColor(v.getTag().toString());
		changeColor(color);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("currentColor", currentColor);
		outState.putInt(STATE_ACTIVE_POSITION, mActivePosition);
        outState.putString(STATE_CONTENT_TEXT, mContentText);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		currentColor = savedInstanceState.getInt("currentColor");
		changeColor(currentColor);
	}

	private Drawable.Callback drawableCallback = new Drawable.Callback() {
		@Override
		public void invalidateDrawable(Drawable who) {
			getActionBar().setBackgroundDrawable(who);
		}

		@Override
		public void scheduleDrawable(Drawable who, Runnable what, long when) {
			handler.postAtTime(what, when);
		}

		@Override
		public void unscheduleDrawable(Drawable who, Runnable what) {
			handler.removeCallbacks(what);
		}
	};
	
	 private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            mActivePosition = position;
	            mMenuDrawer.setActiveView(view, position);
	            
	            mContentTextView.setText(((TextView) view).getText());
	            mMenuDrawer.closeMenu();
	            String selectedPlatform = ((TextView) view).getText().toString();
	            //List<ShortcutKey> refKeys = null;
	            for (Platform vo : platforms)
	            {
	            	if(vo.getPlatformName().trim().equals(selectedPlatform))
	            	{
	            		loadListViewWithAutocomplete(vo.getPlatformKey(), selectedPlatform);
	            		break;
	            	}
	            }
	            AdView adView = (AdView)findViewById(R.id.adView);
	            AdRequest adRequest = new AdRequest();
	            adView.loadAd(adRequest);


	        }
	    };

	    @Override
	    public void onBackPressed() {
	        final int drawerState = mMenuDrawer.getDrawerState();
	        if (drawerState == MenuDrawer.STATE_OPEN || drawerState == MenuDrawer.STATE_OPENING) {
	            mMenuDrawer.closeMenu();
	            return;
	        }

	        super.onBackPressed();
	    }
	private static class Item {

        String mTitle;
        int mIconRes;

        Item(String title, int iconRes) {
            mTitle = title;
            mIconRes = iconRes;
        }
    }

    private static class Category {

        String mTitle;

        Category(String title) {
            mTitle = title;
        }
    }
	private class MenuAdapter extends BaseAdapter {

        private List<Object> mItems;

        MenuAdapter(List<Object> items) {
            mItems = items;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position) instanceof Item ? 0 : 1;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public boolean isEnabled(int position) {
            return getItem(position) instanceof Item;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            Object item = getItem(position);

            if (item instanceof Category) {
                if (v == null) {
                    v = getLayoutInflater().inflate(R.layout.menu_row_category, parent, false);
                }

                ((TextView) v).setText(((Category) item).mTitle);

            } else {
                if (v == null) {
                    v = getLayoutInflater().inflate(R.layout.menu_row_item, parent, false);
                }

                TextView tv = (TextView) v;
                tv.setText(((Item) item).mTitle);
                tv.setCompoundDrawablesWithIntrinsicBounds(((Item) item).mIconRes, 0, 0, 0);
            }

            v.setTag(R.id.mdActiveViewPosition, position);

            if (position == mActivePosition) {
                mMenuDrawer.setActiveView(v, position);
            }

            return v;
        }
    }
	
}